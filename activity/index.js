/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getPersonalInfo(){
		let fullName = prompt("What is your name?");
		let age = prompt("What is your age?");
		let address = prompt("What is your address?");
		alert("Thank you!");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
	}

	getPersonalInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayMyTop5Bands(){
		let myTopBands = ["Cueshé", "Bee Gees", "Simple Plan", "Daughtry", "Imagine Dragons"];
		console.log("1. " + myTopBands[0]);
		console.log("2. " + myTopBands[1]);
		console.log("3. " + myTopBands[2]);
		console.log("4. " + myTopBands[3]);
		console.log("5. " + myTopBands[4]);
	}
	displayMyTop5Bands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayMyTop5Movies(){
		let myTopMovies = [
		["50 First Dates", 45],
		["Polar", 18],
		["No Escape", 48],
		["2012", 39],
		["Dumb And Dumber To", 30]];

		console.log("1. " + myTopMovies[0][0]);
		console.log("Rotten Tomatoes Rating: " + myTopMovies[0][1] + "%");

		console.log("2. " + myTopMovies[1][0]);
		console.log("Rotten Tomatoes Rating: " + myTopMovies[1][1] + "%");

		console.log("3. " + myTopMovies[2][0]);
		console.log("Rotten Tomatoes Rating: " + myTopMovies[2][1] + "%");

		console.log("4. " + myTopMovies[3][0]);
		console.log("Rotten Tomatoes Rating: " + myTopMovies[3][1] + "%");

		console.log("5. " + myTopMovies[4][0]);
		console.log("Rotten Tomatoes Rating: " + myTopMovies[4][1] + "%");
	}
	displayMyTop5Movies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);